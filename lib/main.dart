// ignore_for_file: use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:qr_code_utils/qr_code_utils.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<File> _files = [];
  final pdf = pw.Document();
  String _decoded = 'Unknown';

// Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.builder(
              itemCount: _files.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
              ),
              itemBuilder: (context, index) {
                return displaySelectedFile(_files[index]);
              },
            ),
          ),
          Positioned(
            left: 30.0,
            bottom: 18.0,
            child: ElevatedButton(
              onPressed: () async {
                for (var i = 0; i < _files.length; i++) {
                  if (i == 0) {
                    String? decoded;
                    // Platform messages may fail, so we use a try/catch PlatformException.
                    // We also handle the message potentially returning null.
                    try {
                      decoded = await QrCodeUtils.decodeFrom(_files[0].path) ??
                          'Unknown platform version';
                      _decoded = decoded;
                      print(_decoded);
                    } on PlatformException {
                      _decoded = 'Failed to get decoded.';
                    }

                    // If the widget was removed from the tree while the asynchronous platform
                    // message was in flight, we want to discard the reply rather than calling
                    // setState to update our non-existent appearance.
                    if (!mounted) return;

                    setState(() {
                      // _platformVersion = platformVersion;
                    });
                  }
                  var image = pw.MemoryImage(
                    File(_files[i].path).readAsBytesSync(),
                  );
                  pdf.addPage(pw.Page(
                      pageFormat: PdfPageFormat.a4,
                      build: (pw.Context context) {
                        return pw.Center(child: pw.Image(image));
                      }));
                }
                final output = await getTemporaryDirectory();
                final file = File("${output.path}/example.pdf");
                await file.writeAsBytes(await pdf.save());
              },
              //color: Colors.blue,
              child: const Text(
                'Get PDF',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          XFile? cameraFile;
          //print('Pressed');
          final ImagePicker picker = ImagePicker();
          cameraFile = await picker.pickImage(source: ImageSource.gallery);
          //print(cameraFile.path);
          List<File> temp = _files;
          temp.add(File(cameraFile!.path));
          setState(() {
            _files = temp;
          });
        },
        child: const Icon(Icons.camera_alt),
      ),
    );
  }

  Widget displaySelectedFile(File file) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: SizedBox(
        height: 200.0,
        width: 200.0,
        child: Image.file(file),
      ),
    );
  }
}
